Code and examples related to the article

"Random Forests for Homogeneous and Non-homogeneous Poisson Processes With Excess Zeros"
by Walid Mathlouthi, Denis Larocque and Marc Fredette

Here is a description of the files:

1) using_custom_split_rule_with_randomForestSRC.txt = a general description of how to incorporate custom split rules in the package randomForestSRC.

2) splitCustom.c = example of how to modify the splitCustom.c file in randomForestSRC to add custom split rules.

3) splitCustom.h = example of how to modify the splitCustom.h file in randomForestSRC to add custom split rules.

4) example_homogeneous.R = code example to use the custom split rule in the homogeneous case. 

5) example_non_homogeneous.R = code example to use the custom split rule in the non-homogeneous case.

6) NH.rfsrc.R = wrap-up function to build a random forest in the non-homogeneous case with the specialized split rule.



 