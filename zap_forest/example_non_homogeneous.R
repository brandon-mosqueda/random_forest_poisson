﻿# Example of application of the method described in the paper "Random
# Forests for Homogeneous and Non-homogeneous Poisson Processes With
# Excess Zeros" by Walid Mathlouthi, Denis Larocque and Marc Fredette.

# This is an example for the non-homogeneous case.

# Important: The custom split rule "getCustomSplitNonHomogeneous" must
# be imported in the package randomForestSRC in the Competing risks
# family in the slot 1.
# See the document using
# "using_custom_split_rule_with_randomForestSRC".

# this is the wrap-up function to build the forest
source("NH.rfsrc.R")

# Function required to compute the estimations of the lambdas
g <- function(x, y) x / (1 - exp(-x)) - y

library("randomForestSRC")

set.seed(123456)

# training and test sample sizes
ntrain <- 300
ntest <- 1000
n <- ntrain + ntest

# number of periods
nperiod <- 3

#### Generate the data
# This is basically DGP L with 55% of global 0 but with only the first
# 3 periods (instead of all 12 periods).
# Smaller training and test samples are also used
# because the competings risks family in randomForestSRC takes more
# computing time.

# generate the covariates
x1 <- runif(n, 0, 10)
x2 <- runif(n, 0, 10)
x3 <- runif(n, 0, 10)
x4 <- runif(n, 0, 10)
x5 <- runif(n, 0, 10)
x6 <- runif(n, 0, 10)
x7 <- runif(n, 0, 10)
x8 <- runif(n, 0, 10)
x9 <- runif(n, 0, 10)


# total probability of having a global 0 (around 55%)
theta <- 1 / (1 + exp(-1.1 - 3*log(x1 + 1/2) + 1/5 *
					  (10*x4 - x4^2) + 0.4*x5))

# the Poisson parameters (nperiod parameters per subject)
matlambda <- matrix(0, n, nperiod)

matlambda[, 1] <- (x1 <= 5)*(x2 <= 5)*0.1 + (x1 <= 5)*(x2 > 5)*1.2 +
				  (x1 > 5)*(x3 <= 5)*5.1 + (x1 > 5)*(x3 > 5)*1.2
matlambda[, 2] <- (x1 <= 5)*(x2 <= 5)*2.2 + (x1 <= 5)*(x2 > 5)*1.2 +
				  (x1 > 5)*(x3 <= 5)*0.1 + (x1 > 5)*(x3 > 5)*1.1
matlambda[, 3] <- (x1 <= 5)*(x2 <= 5)*0.3 + (x1 <= 5)*(x2 > 5)*1.2 +
				  (x1 > 5)*(x3 <= 5)*0.1 + (x1 > 5)*(x3 > 5)*1


# sum of the Poisson parameters for each subject
lambda <- apply(matlambda, 1, sum)

# probability of having an excess global 0
p <- (theta - exp(-lambda)) / (1 - exp(-lambda)) *
	 	 ((theta - exp(-lambda)) / (1 - exp(-lambda)) > 0)

# generate the nperiod responses
maty <- matrix(0, n, nperiod)
y <- rep(0, n)
for (i in 1:n) {
	iszero <- rbinom(1, 1, theta[i])

	if (iszero == 1) {
		for (j in 1:nperiod) {
			maty[i, j] <- 0
			y[i] <- 0
		}
	}

	if (iszero == 0) {
		while (y[i] == 0) {
			for(j in 1:nperiod) {
				maty[i,j] <- rpois(1, matlambda[i, j])
				y[i] <- sum(maty[i, ])
			}
		}
	}
}

# generate a binary outcome (y is 0 or not, i.e. there is no event at
# all for this subject)
y0 <- y == 0

dat <- data.frame(cbind(maty, y, y0, x1, x2, x3, x4, x5, x6, x7, x8,
												x9, matlambda, lambda, theta, p))
names(dat) <- c(paste("y", 1:nperiod, sep=""),
								"y", "y0", paste("x", 1:9, sep=""),
								paste("lambda", 1:nperiod, sep=""),
								"lambda", "theta", "p")
summary(dat)

# split into a training and test data sets
datrain <- dat[1:ntrain, ]
datest <- dat[(ntrain+1):n, ]

#### Fit the models

# fit a forest with y0 (zero model), get the estimations of theta for
# the test set, plot the predictions vs the true values, and compute
# the MSE.
rf0 <- rfsrc(y0 ~ x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9,
						 ntree=500, data=datrain)
thetahat <- predict(rf0, newdata=datest)$predicted
plot(datest$theta, thetahat)
mean((thetahat - datest$theta)^2)

# fit a forest with the observations such that y>0 with the custom
# split rule (truncated model), compute the estimations of the lambdas
# for the test set.
xnames <- paste("x", 1:9, sep="")
ynames <- paste("y", 1:nperiod, sep="")

# In our case, the Yi will contain the number of events in each
# period. and datrain is the subset of data such that at least one yi
# is > 0.
datrain0 <- datrain[datrain$y > 0, ]

# NH.rfsrc is the wrap-up function to build the RF with the
# specialized split rule in the NH case
matyhat0 <- NH.rfsrc(ynames, xnames, datrain0, datest, ntrees=500)

yhat0 <- apply(matyhat0, 1, sum)

# compute the estimation of lambda = sum of the individual lambdas.
lambdahat <- rep(0, ntest)
for (i in 1:ntest) {
	lambdahat[i] <- uniroot(g, interval=c(-1, yhat0[i] + 2),
													y=yhat0[i])$root
	lambdahat[i] <- lambdahat[i] * (lambdahat[i] > 0)
}

# Compute the estimations of the individual lambdas (one for each
# period).
matlambdahat <- matrix(0, ntest, nperiod)
for (i in 1:ntest) {
	for (j in 1:nperiod) {
		matlambdahat[i, j] <- matyhat0[i, j] * (1 - exp(-lambdahat[i]))
	}
}

matlambdahat <- data.frame(matlambdahat)
names(matlambdahat) <- paste("lambdahat", 1:nperiod, sep="")

# Compute the mean of the MSE for all the lambdas
mean((stack(datest[, paste("lambda", 1:nperiod, sep="")])[, 1] -
		  stack(matlambdahat)[, 1])^2)

# Compute the estimations of p for the test set, plot the predictions
# vs the true values and compute the MSE.
phat <- (thetahat - exp(-lambdahat)) / (1 - exp(-lambdahat)) *
				((thetahat - exp(-lambdahat)) / (1 - exp(-lambdahat)) > 0)
plot(datest$p, phat)
mean((datest$p - phat)^2)

# Use the default multivariate split rule instead of the specialized
# one for the truncated model.

rftruncls <- rfsrc(Multivar(y1, y2, y3, y4, y5, y6, y7, y8,
														y9, y10, y11, y12) ~
									 x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9,
									 ntree=500, nodesize=30, data=datrain0)
rftruncls <- rfsrc(Multivar(y1, y2, y3) ~
									 x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9,
									 ntree=500, nodesize=30, data=datrain0)
pred0ls <- predict(rftruncls, newdata=datest)

yhat0ls <- pred0ls$regrOutput$y1$predicted +
					 pred0ls$regrOutput$y2$predicted +
					 pred0ls$regrOutput$y3$predicted

# compute the estimation of lambda = sum of the individual lambdas.
lambdahatls <- rep(0, ntest)
for (i in 1:ntest) {
	lambdahatls[i] <- uniroot(g, interval=c(-1, yhat0ls[i] + 2),
														y=yhat0ls[i])$root
	lambdahatls[i] <- lambdahatls[i] * (lambdahatls[i] > 0)
}

# Compute the estimations of the individual lambdas (one for each
# period).
matlambdahatls <- matrix(0, ntest, nperiod)
for (i in 1:ntest) {
	matlambdahatls[i, 1] <- pred0ls$regrOutput$y1$predicted[i] *
													(1 - exp(-lambdahatls[i]))
	matlambdahatls[i, 2] <- pred0ls$regrOutput$y2$predicted[i] *
													(1 - exp(-lambdahatls[i]))
	matlambdahatls[i, 3] <- pred0ls$regrOutput$y3$predicted[i] *
													(1 - exp(-lambdahatls[i]))
}

matlambdahatls <- data.frame(matlambdahatls)
names(matlambdahatls) <- paste("lambdahatls", 1:nperiod, sep="")

# Compute the mean of the MSE for all the lambdas
mean((stack(datest[, paste("lambda", 1:nperiod, sep="")])[, 1] -
			stack(matlambdahatls)[, 1])^2)

# Compute the estimations of p for the test set, plot the predictions
# vs the true values and compute the MSE.
phatls <- (thetahat - exp(-lambdahatls)) / (1 - exp(-lambdahatls)) *
					((thetahat - exp(-lambdahatls)) /
					 (1 - exp(-lambdahatls)) > 0)
plot(datest$p, phatls)
mean((datest$p - phatls)^2)