rm(list=ls())

library(glmnet)
library(BMTME)

load("Final_data.RData")
ls()

str(enviroments)
summary(enviroments)
length(enviroments)

dim(X)

summary(y)
dim(y)

########### Outer Cross-validation ###########
digits <- 4
N <- nrow(X)
partitions_num <- 5
traits_names <- colnames(y)

Pred_All <- data.frame()

for (trait_name in traits_names) {
  inner_y <- y[[trait_name]]

  results <- data.frame()

  for(k in 1:partitions_num) {
    set.seed(k)

    tst_set <- sample(N, 0.2 * N)

    Env_test <- enviroments[tst_set]
    X_trn <- X[-tst_set, ]
    X_tst <- X[tst_set, ]
    y_trn <- inner_y[-tst_set]
    y_tst <- inner_y[tst_set]

    model <- cv.glmnet(X_trn, y_trn, type.measure='mse',
                       family='poisson', alpha=0)

    predictions <- predict(model, newx=X_tst,
                           s='lambda.min', type="response")
    predictions <- c(predictions)

    results <- rbind(results,
                     data.frame(Position=tst_set,
                                Environment=Env_test,
                                Partition=k,
                                Observed=round(y_tst, digits),
                                Predicted=round(predictions, digits),
                                Trait=trait_name))
    cat("CV =", k, "\n")
  }

  Pred_All <- rbind(Pred_All, results)
}

write.csv(Pred_All, file="MT_Count_WI_Poison_glmnet_Ridge.csv")
head(Pred_All)

# Mean square error of prediction
mse <- function(actual, predicted) mean((actual - predicted) ^ 2)

mse(Pred_All$Observed, Pred_All$Predicted)