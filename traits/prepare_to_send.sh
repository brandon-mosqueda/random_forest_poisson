DEST=~/Downloads/results_zap_random_forest
ORIGIN=~/Data_Science/random_forest_poisson/traits

rm -rf $DEST
rm -f $DEST/results_zap_random_forest.zip

mkdir -p $DEST/dataset1 $DEST/dataset2

cp $ORIGIN/benchmarking/summary_by_env.csv $DEST
cp $ORIGIN/benchmarking/summary_by_model.csv $DEST

cp $ORIGIN/benchmarking/real_count/results/importances* $DEST/dataset1
cp $ORIGIN/benchmarking/nery_count/results/all_importances* $DEST/dataset2

cp -r $ORIGIN/plots/plots_dataset1/* $DEST/dataset1
cp -r $ORIGIN/plots/plots_dataset2/* $DEST/dataset2

cd $DEST
cd ..
zip -r results_zap_random_forest.zip results_zap_random_forest -q