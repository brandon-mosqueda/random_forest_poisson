#!/bin/bash

#PBS -N tune_zap_svm_nery_sb_no_interaction.R
#PBS -l nodes=1:ppn=64,mem=30gb
#PBS -q staff
#PBS -d /mnt/zfs-pool/home/osval.montes/Osval/Brandon/random_forest_poisson/run
#PBS -o tune_zap_svm_nery_sb_no_interaction.log
#PBS -j oe
#PBS -V
#PBS -S /bin/bash
#PBS -m ea                    # Send mail on end and abort
#MAILTO="bmosqueda@ucol.mx"   # default is the user submitting the job

# Habilitar conda
source ~/anaconda3/etc/profile.d/conda.sh

# Habilitar env conda
conda activate tf_2

# Correr script
R CMD BATCH tune_zap_svm_nery_sb_no_interaction.R