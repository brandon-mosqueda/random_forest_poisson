rm(list=ls())

library(BMTME)

load("Data_Multi_Count_Real.RData")

Data_count_trait1 <- Count.Data[Count.Data$Trait == "Trait1", ]
dim(Data_count_trait1)

Data_count_trait2 <- Count.Data[Count.Data$Trait == "Trait2", ]
dim(Data_count_trait2)
head(Data_count_trait2)

Pheno_MT_Count <- data.frame(GID=Data_count_trait1$GID,
                             Env=Data_count_trait1$Env,
                             y1=Data_count_trait1$y,
                             y2=Data_count_trait2$y)
head(Pheno_MT_Count)
tail(Pheno_MT_Count)

G_Count <- Gg.C

########### Ordering the data ###########
Pheno_MT_Count <- Pheno_MT_Count[order(Pheno_MT_Count$Env,
                                       Pheno_MT_Count$GID), ]
rownames(Pheno_MT_Count) <- 1:nrow(Pheno_MT_Count)
head(Pheno_MT_Count)

########### Design matrices ###########
ZG <- model.matrix(~0 + as.factor(Pheno_MT_Count$GID))
# GIDs <- paste("L", unique(Pheno_MT_Count$GID), sep="")
GIDs <- unique(Pheno_MT_Count$GID)
GIDs
Pos <- match(GIDs, colnames(G_Count))
Pos
G_Count <- G_Count[Pos, Pos]
LG <- cholesky(G_Count)
Z.G <- ZG %*% LG
Z.E <- model.matrix(~0 + as.factor(Pheno_MT_Count$Env))
ZEG <- model.matrix(~0 + Z.G:as.factor(Pheno_MT_Count$Env))
Z.EG <- ZEG
dim(Z.EG)
dim(Z.G)

########### Selecting the response variable ###########
Data_Pheno <- Pheno_MT_Count
head(Data_Pheno)

########### X training and testing ###########
X <- cbind(Z.E, Z.G)
dim(X)

y <- Pheno_MT_Count[, 3:4]
length(y)
head(y)
tail(y)
summary(y)

environments <- Data_Pheno$Env

save(X, y, environments, file="processed/Data_Multi_Count_Real.RData")