rm(list=ls(all=TRUE))

setwd("~/Data_Science/random_forest_poisson/traits/tuning/")

interaction_names <- c("_interaction", "_no_interaction")

for (interaction_name in interaction_names) {
  ############################ REAL FLAGS INTERACTION ##########################
  RealFlags <- list()

  load(paste0("results/Data_Real_Count/flags", interaction_name,".RData"),
       verbose=TRUE)
  RealFlags$hzap <- best_params

  load(paste0("results/Data_Real_Count/random_forest/flags", interaction_name, ".RData"),
       verbose=TRUE)
  RealFlags$random_forest <- best_params

  load(paste0("results/Data_Real_Count/svm/flags", interaction_name, ".RData"),
       verbose=TRUE)
  RealFlags$svm <- best_params

  load(paste0("results/Data_Real_Count/svm/zap_flags", interaction_name, ".RData"),
      verbose=TRUE)
  RealFlags$zap_svm <- best_params

  save(RealFlags, file=paste0("results/Data_Real_Count/real_flags", interaction_name, ".RData"))

  ############
  NeryFlags <- list(SN=list(), SB=list(), PTR=list())

  load(paste0("results/Data_Nery_Count/flags_SN", interaction_name, ".RData"),
       verbose=TRUE)
  NeryFlags$SN$hzap <- best_params

  load(paste0("results/Data_Nery_Count/random_forest/flags_SN", interaction_name, ".RData"), verbose=TRUE)
  NeryFlags$SN$random_forest <- best_params

  load(paste0("results/Data_Nery_Count/svm/flags_SN", interaction_name, ".RData"), verbose = TRUE)
  NeryFlags$SN$svm <- best_params

  load(paste0("results/Data_Nery_Count/svm/zap_flags_SN", interaction_name, ".RData"), verbose = TRUE)
  NeryFlags$SN$zap_svm <- best_params


  load(paste0("results/Data_Nery_Count/flags_SB", interaction_name, ".RData"), verbose=TRUE)
  NeryFlags$SB$hzap <- best_params

  load(paste0("results/Data_Nery_Count/random_forest/flags_SB", interaction_name, ".RData"), verbose=TRUE)
  NeryFlags$SB$random_forest <- best_params

  load(paste0("results/Data_Nery_Count/svm/flags_SB", interaction_name, ".RData"), verbose = TRUE)
  NeryFlags$SB$svm <- best_params

  load(paste0("results/Data_Nery_Count/svm/zap_flags_SB", interaction_name, ".RData"), verbose = TRUE)
  NeryFlags$SB$zap_svm <- best_params


  load(paste0("results/Data_Nery_Count/flags_PTR", interaction_name, ".RData"), verbose=TRUE)
  NeryFlags$PTR$hzap <- best_params

  load(paste0("results/Data_Nery_Count/random_forest/flags_PTR", interaction_name, ".RData"), verbose=TRUE)
  NeryFlags$PTR$random_forest <- best_params

  load(paste0("results/Data_Nery_Count/svm/flags_PTR", interaction_name, ".RData"), verbose = TRUE)
  NeryFlags$PTR$svm <- best_params

  load(paste0("results/Data_Nery_Count/svm/zap_flags_PTR", interaction_name, ".RData"), verbose = TRUE)
  NeryFlags$PTR$zap_svm <- best_params

  save(NeryFlags, file=paste0("results/Data_Nery_Count/nery_flags", interaction_name, ".RData"))
}